from pyVmomi import vmodl
from pyVmomi import vim
import requests

def get_vcenter_datastore_info(vcenter_host, vcenter_user, vcenter_pass, validate_certs=True):
    try:
        # Connect to vCenter server
        service_instance = vim.ServiceInstance("https://{}/sdk".format(vcenter_host),
                                               username=vcenter_user,
                                               password=vcenter_pass,
                                               ignore_ssl_warning=not validate_certs)
        content = service_instance.RetrieveContent()

        # Get the list of datastores
        datastore_view = content.viewManager.CreateContainerView(content.rootFolder,
                                                                  [vim.Datastore],
                                                                  True)
        datastores = datastore_view.view

        # Collect statistics for each datastore
        for datastore in datastores:
            print("Datastore: {}".format(datastore.name))
            print("\tCapacity: {} GB".format(datastore.summary.capacity / 1024**3))
            print("\tFree Space: {} GB".format(datastore.summary.freeSpace / 1024**3))
            print("\tUncommitted Space: {} GB".format(datastore.summary.uncommitted / 1024**3))

    except vmodl.MethodFault as error:
        print("Caught vmodl fault : " + error.msg)
        return -1

    except Exception as error:
        print("Caught exception : " + str(error))
        return -1

# Main function
if __name__ == '__main__':
    # Replace with your vCenter server information
    vcenter_host = "vcenter.example.com"
    vcenter_user = "username"
    vcenter_pass = "password"

    get_vcenter_datastore_info(vcenter_host, vcenter_user, vcenter_pass)
