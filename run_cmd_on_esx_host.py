import paramiko

# Set the ESX host IP address and username/password
hostname = "192.168.1.100"
username = "root"
password = "password"

# Set the command to run on the ESX host
command = "ls -l"

# Create an instance of the SSH client
ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

# Connect to the ESX host
ssh.connect(hostname, username=username, password=password)

# Run the command on the ESX host
stdin, stdout, stderr = ssh.exec_command(command)

# Print the output of the command
print(stdout.read().decode())

# Close the SSH connection
ssh.close()
